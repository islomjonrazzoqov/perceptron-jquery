var matrix=[
    [0,0,0,1,0,0,0,0],
    [0,0,1,1,0,0,0,0],
    [0,1,0,1,0,0,0,0],
    [0,0,0,1,0,0,0,0],
    [0,0,0,1,0,0,0,0],
    [0,0,0,1,0,0,0,0],
    [0,0,0,1,0,0,0,0],
    [0,0,0,1,0,0,0,0]
]
var weight=[
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0]
]
var output=[
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0]
]
var y=0, tetta=0.001,epsilon=0, inputObraz=[];
var flag=true;

$(document).ready(function(){
    createTable();
    tableWidth();
    read_Obraz_Table();

    var clearInputBtn=$("#clearTable");
    clearInputBtn.click(function(){
        clearTable(matrix);
        createTable();
    })

    var clearWeightBtn=$("#clearWeightBtn");
    clearWeightBtn.click(function(){
        clearTable(weight);
        tableWidth();
    })

    var clearOutputBtn=$("#clearOutput");
    clearOutputBtn.click(function(){
        clearTable(output);
        read_Obraz_Table();
    })
    
    $('select').on('change', function() {
        y=this.value;
    });

    var readBtn=$("#readTable");
    readBtn.click(function(){
        addInput(y);
    })

    var calculateWeightBtn=$("#calculateWeight");
    calculateWeightBtn.click(function(){
        var outputCard=$(".output");
        outputCard.css({"display":"block"});
        while(flag){
            flag=false;
            inputObraz.forEach(item=>{
                var obraz=item[0];
                var y=item[1];
                flag=calcWeight(obraz,y);
            })
        }
        tableWidth();
    })   
    
    var readTableBtn=$("#calcObraz");
    readTableBtn.click(function(){
        calcObraz();
    })
})
function createTable(){
    var div=document.querySelector(".table-input");
    div.innerHTML='';
    var tr = d3.select(".table-input").append("table").attr('class','input').selectAll("tr")
    .data(matrix)
    .enter().append("tr");

    tr.selectAll("td")
    .data(function(d) {return d; })
    .enter().append("td")
    .on("click", function() { 
        var row=this.closest('tr').rowIndex;
        var cell = this.cellIndex;
        clicked(matrix,row,cell)
    })
    .text(function(d) {
        if(d===1){
               this.setAttribute("bgcolor","#f00");d='';
        }
        else {
             this.setAttribute("bgcolor","yellow");d='';
        }
        return d; 
    });
}
function clicked(matrix,row,cell){
    if (matrix[row][cell] === 1) matrix[row][cell] = 0;
      else matrix[row][cell] = 1;
    matrix=matrix;
    createTable();
}
function addInput(y){ 
    var a = [];
    for (var i = 0; i < this.matrix.length; i++)
      a[i] = this.matrix[i].slice();
    this.inputObraz.push([a, Number(y)]);
}

function signX(matrix){
    var result=0;
    for(var i=0;i<matrix.length;i++){
        for(var j=0;j<matrix.length;j++){
            result+=matrix[i][j]*weight[i][j]-tetta;
        }
    }
    if(result>epsilon)return 1;
    else return 0;
}

function calcWeight(obraz,y){
    while(true){
        var fx=signX(obraz);
        if(fx<=epsilon&&y===1){
             flag=true;
            for(var i=0;i<obraz.length;i++){
                for(var j=0;j<obraz.length;j++){
                    weight[i][j]=weight[i][j]+obraz[i][j];
                }
            }
            weight=weight;
        }
        else if(fx>epsilon&&y===0){
            flag=true;
            for(var i=0;i<obraz.length;i++){
                for(var j=0;j<obraz.length;j++){
                    weight[i][j]=weight[i][j]-obraz[i][j];
                }
            }
            weight=weight;
        }
        else break;
    }
    return flag;
}

function clearTable(matrix){
    for (let index = 0; index < 8; index++) {
        for (let cellindex = 0; cellindex < 8; cellindex++) {
          matrix[index][cellindex] = 0;
        }
    }
}
function tableWidth(){
    var div=document.querySelector(".table-weight");
    div.innerHTML='';
    var tr = d3.select(".table-weight").append("table").attr('class','table table-bordered weight').selectAll("tr")
    .data(weight)
    .enter().append("tr");

    tr.selectAll("td")
    .data(function(d) {return d; })
    .enter().append("td")
    .text(function(d) {
        switch(d){
            case d<-2:this.setAttribute("bgcolor","#F80008");break;
            case -2: this.setAttribute("bgcolor","#F88688");break;
            case -1: this.setAttribute("bgcolor","#FAC1C3");break;
            case 0: this.setAttribute("bgcolor","#FFF9FF");break;
            case 1: this.setAttribute("bgcolor","#CCDAEE");break;
            case 2: this.setAttribute("bgcolor","#7BA1D2");break;
            default: this.setAttribute("bgcolor","#2D37FF");break;
        }
        return d; 
    });
}
function read_Obraz_Table(){
    var div=document.querySelector(".table-obraz");
    div.innerHTML='';
    var tr = d3.select(".table-obraz").append("table").attr('class','input').selectAll("tr")
    .data(output)
    .enter().append("tr");

    tr.selectAll("td")
    .data(function(d) {return d; })
    .enter().append("td")
    .on("click", function() { 
        var row=this.closest('tr').rowIndex;
        var cell = this.cellIndex;
        clickedRead(output,row,cell)
    })
    .text(function(d) {
        if(d===1){
               this.setAttribute("bgcolor","#f00");d='';
        }
        else {
             this.setAttribute("bgcolor","yellow");d='';
        }
        return d; 
    });
}
function clickedRead(matrix,row,cell){
    if (matrix[row][cell] === 1) matrix[row][cell] = 0;
      else matrix[row][cell] = 1;
    output=matrix;
    read_Obraz_Table();
}
var toast=document.getElementById("toast-info");

function calcObraz(){
    var fx= this.signX(output);
    if (fx == 0) {
        toast.innerText="Toq";
    } else {
        toast.innerText="Juft";
    }
    toastInfo();
}

function toastInfo(){
    toast.className="show";
    setTimeout(function(){
        toast.className=toast.className.replace("show","");
    },3000)
 }